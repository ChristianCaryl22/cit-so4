package com.zuitt;

import java.io.IOException;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1804323570948502389L;


	public void init() throws ServletException{
	        System.out.println("**********************************");
	        System.out.println(" UserServlet has been initialized ");
	        System.out.println("**********************************");
	    }

	    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
	        String firstname = req.getParameter("firstname");
	        System.getProperties().put("firstname", firstname);
	        String lastname =  req.getParameter("lastname");
	       
	        HttpSession Lname = req.getSession();
	        Lname.setAttribute("lastname", lastname);
	        
	        String email =  req.getParameter("email");
	        ServletContext srvcontext = getServletContext();
	        srvcontext.setAttribute("email", email);
	        
	        String contact =  req.getParameter("contact");
	        res.sendRedirect("details?contact="+ contact);
	        		

	    }
	    
	    
	    public void destroy() {
			System.out.println("**********************************");
			System.out.println(" UserServlet has been destroyed ");
			System.out.println("**********************************");
		}

	}
